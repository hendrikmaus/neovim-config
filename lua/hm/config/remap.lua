-- Set <space> as the leader key
vim.g.mapleader = " "
vim.g.maplocalleader = " "

-- Shorten function name
local keymap = vim.keymap.set

-- Modes
--   normal_mode = "n",
--   insert_mode = "i",
--   visual_mode = "v",
--   visual_block_mode = "x",
--   term_mode = "t",
--   command_mode = "c",

------------------- NORMAL MODE -----------------------------------------------
-- quit all
keymap('n', '<leader>qq', ':qa<CR>')
keymap('n', '<leader>qf', ':qa!<CR>')

-- Save with Ctrl-s
keymap('n', '<C-s>', ':w<CR>')

-- Redo with U
keymap("n", "U", "<C-r>", { silent = true, desc = "Redo" })

-- Remap for dealing with word wrap
keymap('n', 'k', "v:count == 0 ? 'gk' : 'k'", { expr = true, silent = true })
keymap('n', 'j', "v:count == 0 ? 'gj' : 'j'", { expr = true, silent = true })

-- Open netrw
keymap("n", "<leader>pv", "<cmd>Ex %:p:h<CR>")

-- Buffers navigate
keymap("n", "<Tab>", ":bn<CR>", { silent = true, desc = "next tab" })
keymap("n", "<S-Tab>", ":bp<CR>", { silent = true, desc = "prev tab" })

-- keep cursor where it is when using J to append the next line to the current one
keymap("n", "J", "mzJ`z")

-- keep cursor in the middle when jumping half pages
keymap("n", "<C-d>", "<C-d>zz")
keymap("n", "<C-u>", "<C-u>zz")

-- keep search terms in the middle
keymap("n", "n", "nzzzv")
keymap("n", "N", "Nzzzv")

-- make the current file executable
-- this is handy as it saves going back to the command line to chmod +x
keymap("n", "<leader>x", "<cmd>!chmod +x %<CR>", { silent = true })

-- Split windows
keymap("n", "vs", ":vs<CR>", { silent = true, desc = "split vertically" })
keymap("n", "sp", ":sp<CR>", { silent = true, desc = "split horizontally" })

-- Window navigation
keymap({ "n", "i" }, "<C-h>", "<C-w>h", { silent = true, desc = "Go to left window" })
keymap({ "n", "i" }, "<C-j>", "<C-w>j", { silent = true, desc = "Go to lower window" })
keymap({ "n", "i" }, "<C-k>", "<C-w>k", { silent = true, desc = "Go to upper window" })
keymap({ "n", "i" }, "<C-l>", "<C-w>l", { silent = true, desc = "Go to right window" })

-- Resize
keymap("n", "<C-Up>", ":resize +2<CR>", { silent = true, desc = "Increase window height" })
keymap("n", "<C-Down>", ":resize -2<CR>", { silent = true, desc = "Decrease window height" })
keymap("n", "<C-Left>", ":vertical :resize -2<CR>", { silent = true, desc = "Decrease window width" })
keymap("n", "<C-Right>", ":vertical :resize +2<CR>", { silent = true, desc = "Increase window width" })

-- Diagnostic keymaps
keymap('n', '[d', vim.diagnostic.goto_prev, { desc = 'Go to previous [D]iagnostic message' })
keymap('n', ']d', vim.diagnostic.goto_next, { desc = 'Go to next [D]iagnostic message' })
keymap('n', '<leader>de', vim.diagnostic.open_float, { desc = 'Show diagnostic [E]rror messages' })
keymap('n', '<leader>dq', vim.diagnostic.setloclist, { desc = 'Open diagnostic [Q]uickfix list' })

-- Disable command history
keymap('n', 'q', '<nop>')

------------------- INSERT MODE -----------------------------------------------

-- Map CTRL+c to ESC to avoid weirdness
keymap("i", "<C-c>", "<Esc>")

-- Save with Ctrl-s and leave insert mode
keymap('i', '<C-s>', '<Esc>:w<CR>')
-- keymap('i', '<C-s>', '<C-o>:w<CR>')

------------------- VISUAL MODE -----------------------------------------------

-- use J and K to move highlighted blocks around
keymap("v", "J", ":m '>+1<CR>gv=gv")
keymap("v", "K", ":m '<-2<CR>gv=gv")

-- Stay in indent mode
keymap("v", "<", "<gv", { silent = true })
keymap("v", ">", ">gv", { silent = true })

------------------- VISUAL BLOCK MODE -----------------------------------------------

-- do not replace the paste register when pasting over something
keymap("x", "<leader>p", [["_dP]])
