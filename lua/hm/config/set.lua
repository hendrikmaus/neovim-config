-- [[ Setting options ]]
-- See `:help vim.o`

-- Netrw config
--   don't display the banner
--   simulate some tree-like behavior
--   make window smaller for Hexplore and Vexplore
vim.g.netrw_browse_split = 'P'
vim.g.netrw_banner = 0
vim.g.netrw_winsize = 25
vim.g.netrw_liststyle = 3
vim.g.netrw_preview = 1
-- vim.g.netrw_keepdir = 0

-- auto reload buffers
vim.o.autoread = true

-- Set highlight on search
vim.o.hlsearch = false

-- Set incremental search
vim.opt.incsearch = true

-- Make line numbers default
vim.wo.number = true
-- line numbers + relative appearance
vim.o.nu = true
vim.o.relativenumber = true

-- indents
vim.opt.tabstop = 4
vim.opt.softtabstop = 4
vim.opt.shiftwidth = 4
vim.opt.expandtab = true
vim.opt.smartindent = true

-- swap, backup and undo file handling
vim.opt.swapfile = false
vim.opt.backup = false
vim.opt.undodir = os.getenv("HOME") .. "/.vim/undodir"
vim.opt.undofile = true

-- views can only be fully collapsed with the global statusline
vim.opt.laststatus = 3

-- do not wrap long lines
vim.o.wrap = false

-- always keep 8 lines at the bottom when scrolling down
vim.o.scrolloff = 8

-- Enable mouse mode
vim.o.mouse = 'a'

-- Sync clipboard between OS and Neovim.
-- See `:help 'clipboard'`
vim.o.clipboard = 'unnamedplus'

-- Enable break indent
vim.o.breakindent = true

-- Case-insensitive searching UNLESS \C or capital in search
vim.o.ignorecase = true
vim.o.smartcase = true

-- Keep signcolumn on by default
vim.wo.signcolumn = 'yes'

-- Decrease update time
-- causes a faster perceptionional speed
vim.o.updatetime = 50
vim.o.timeoutlen = 1000

-- Set completeopt to have a better completion experience
vim.o.completeopt = 'menuone,noselect'

-- Use rich colors
vim.o.termguicolors = true

-- Highlight column number 80
vim.opt.colorcolumn = "80"

-- Spellchecking
vim.opt.spell = true
vim.opt.spelllang = { 'en_us' }

-- Add additional filetype mappings
vim.filetype.add({
  pattern = {
    ['.env*'] = 'conf',
    ['*.nomad'] = 'hcl',
    ['*.tfvars'] = 'hcl',
  },
})
