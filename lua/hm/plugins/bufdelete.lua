return {
    "famiu/bufdelete.nvim",
    config = function()
        vim.keymap.set("n", "<leader>bd", vim.cmd.Bdelete, { desc = "[b]uffer [d]elete" })
    end
}
