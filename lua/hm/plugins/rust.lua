return {
  {
    'mrcjkb/rustaceanvim',
    version = '^5',
    lazy = false,
    ft = { 'rust' },
    dependencies = {
      "nvim-lua/plenary.nvim",
      "mfussenegger/nvim-dap",
      {
        "lvimuser/lsp-inlayhints.nvim",
        opts = {}
      },
    },
    config = function()
      local lsp = require("hm.utils.lsp")
      vim.g.rustaceanvim = {
        inlay_hints = {
          highlight = "NonText",
        },

        -- Plugin configuration
        tools = {
          hover_actions = {
            auto_focus = true,
          },
        },

        -- LSP configuration
        server = {
          on_attach = function(client, bufnr)
            -- attach common lsp functionality
            lsp.on_attach(client, bufnr)

            -- inlay hint support in neovim <0.10
            require("lsp-inlayhints").on_attach(client, bufnr)

            -- keymaps
            vim.keymap.set(
              "n",
              "<leader>a",
              function()
                vim.cmd.RustLsp('codeAction') -- supports rust-analyzer's grouping
                -- or vim.lsp.buf.codeAction() if you don't want grouping.
              end,
              { silent = true, buffer = bufnr }
            )

            vim.keymap.set(
              "n",
              "<leader>dd",
              function()
                vim.cmd.RustLsp('debuggables')
              end
            )
          end,
          default_settings = {
            -- rust-analyzer language server configuration
            ['rust-analyzer'] = {
              diagnostics = { enable = true },
              rustfmt = {
                extraArgs = { "+nightly" },
              },
            },
          },
        },

        -- DAP configuration
        dap = {
          -- see dap.lua
        },
      }
    end
  },

  -- Work wirh Crates.io https://github.com/saecki/crates.nvim
  {
    'saecki/crates.nvim',
    tag = 'stable',
    config = function()
      require('crates').setup({
        popup = {
          autofocus = true,
        },
        lsp = {
          enabled = true,
          on_attach = function(client, bufnr)
            local crates = require("crates")
            local opts = { silent = true }

            vim.keymap.set("n", "<leader>ct", crates.toggle, { desc = "Toogle crates", silent = true })
            vim.keymap.set("n", "<leader>cr", crates.reload, { desc = "Reload crates", silent = true })
            vim.keymap.set("n", "<leader>cp", crates.show_popup, { desc = "Show crate popup", silent = true })

            vim.keymap.set("n", "<leader>cv", crates.show_versions_popup, { desc = "Show crate versions", silent = true })
            vim.keymap.set("n", "<leader>cf", crates.show_features_popup, { desc = "Show crate features", silent = true })
            vim.keymap.set("n", "<leader>cd", crates.show_dependencies_popup,
              { desc = "Show crate dependencies", silent = true })

            vim.keymap.set("n", "<leader>cu", crates.update_crate, { desc = "Update crate", silent = true })
            vim.keymap.set("v", "<leader>cu", crates.update_crates, { desc = "Update crates", silent = true })
            vim.keymap.set("n", "<leader>ca", crates.update_all_crates, { desc = "Update all crates", silent = true })
            vim.keymap.set("n", "<leader>cU", crates.upgrade_crate, { desc = "Upgrade crate", silent = true })
            vim.keymap.set("v", "<leader>cU", crates.upgrade_crates, { desc = "Upgrade crates", silent = true })
            vim.keymap.set("n", "<leader>cA", crates.upgrade_all_crates, { desc = "Upgrade all crates", silent = true })

            vim.keymap.set("n", "<leader>cx", crates.expand_plain_crate_to_inline_table,
              { desc = "To inline table", silent = true })
            vim.keymap.set("n", "<leader>cX", crates.extract_crate_into_table, { desc = "To table", silent = true })

            vim.keymap.set("n", "<leader>cH", crates.open_homepage, { desc = "Crate homepage", silent = true })
            vim.keymap.set("n", "<leader>cR", crates.open_repository, { desc = "Crate repository", silent = true })
            vim.keymap.set("n", "<leader>cD", crates.open_documentation, { desc = "Crate documentation", silent = true })
            vim.keymap.set("n", "<leader>cC", crates.open_crates_io, { desc = "Crates.io page", silent = true })
            vim.keymap.set("n", "<leader>cL", crates.open_lib_rs, { desc = "Lib.rs page", silent = true })
          end,
          actions = true,
          completion = true,
          hover = true,
        },
      })
    end,
  }
}
