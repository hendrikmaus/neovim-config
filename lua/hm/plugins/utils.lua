return {
    -- detect tabstop and shiftwidth automatically
    { 'tpope/vim-sleuth' },

    -- get readline bindings, e.g. C-f to move a character forward in insert mode
    { 'tpope/vim-rsi' },

    -- Useful status updates for LSP
    -- NOTE: `opts = {}` is the same as calling `require('fidget').setup({})`
    { 'j-hui/fidget.nvim', opts = {} },

    -- Add indentation guides even on blank lines
    {
        'lukas-reineke/indent-blankline.nvim',
        -- Enable `lukas-reineke/indent-blankline.nvim`
        -- See `:help ibl`
        main = 'ibl',
        opts = {},
    },

    -- "gc" to comment visual regions/lines
    { 'numToStr/Comment.nvim',   opts = {} },

    -- TODO, NOTE etc. comment highlighting
    {
        "folke/todo-comments.nvim",
        dependencies = { "nvim-lua/plenary.nvim" },
        opts = {
            -- your configuration comes here
            -- or leave it empty to use the default settings
            -- refer to the configuration section below
        }
    },

    -- Which key for showing available keybindings
    {
        "folke/which-key.nvim",
        event = "VeryLazy",
        init = function()
            vim.o.timeout = true
            vim.o.timeoutlen = 300
        end,
        opts = {
        }
    },

    -- UI improvements
    {
        'stevearc/dressing.nvim',
        opts = {},
    },

    -- UI components
    { "MunifTanjim/nui.nvim",    lazy = true },

    -- Lists in Markdown
    { "bullets-vim/bullets.vim", lazy = true, ft = "markdown" },
}
