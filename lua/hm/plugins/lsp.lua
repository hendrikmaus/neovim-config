return {
    -- LSP Configuration & Plugins
    'neovim/nvim-lspconfig',
    dependencies = {
        -- Automatically install LSPs to stdpath for neovim
        { 'williamboman/mason.nvim', config = true },
        'williamboman/mason-lspconfig.nvim',

        -- Useful status updates for LSP
        -- NOTE: `opts = {}` is the same as calling `require('fidget').setup({})`
        { 'j-hui/fidget.nvim',       opts = {} },

        -- Additional lua configuration, makes nvim stuff amazing!
        'folke/neodev.nvim',
    },
    config = function()
        local lsp = require("hm.utils.lsp")

        -- mason-lspconfig requires that these setup functions are called in this order
        -- before setting up the servers.
        require('mason').setup()
        require('mason-lspconfig').setup()

        local servers = {
            ansiblels = {},
            bashls = {},
            docker_compose_language_service = {},
            dockerls = {},
            golangci_lint_ls = {},
            gopls = {},
            html = { filetypes = { 'html', 'twig', 'hbs' } },
            jqls = {},
            jsonls = {},
            marksman = {}, -- place .marksman.toml in a project for full feature set
            pyright = {},
            taplo = {},    -- toml
            terraformls = {},
            tflint = {},
            yamlls = {},

            lua_ls = {
                Lua = {
                    workspace = { checkThirdParty = false },
                    telemetry = { enable = false },
                    diagnostics = {
                        disable = { 'missing-fields' },
                        globals = { 'vim' },
                    },
                },
            },
        }

        -- Setup neovim lua configuration
        require('neodev').setup()

        -- Setup Nix LSP
        --
        -- tools that need to be installed for this to work:
        --   nix profile install nixpkgs#nixd
        --   nix profile install nixpkgs#alejandra
        if (os.getenv("NVIM_LSP_NIXD_ENABLED") == "true")
        then
            require('lspconfig').nixd.setup({
                -- https://github.com/nix-community/nixd/blob/main/nixd/docs/configuration.md
                cmd = { "nixd" },
                settings = {
                    nixd = {
                        formatting = {
                            command = { "alejandra" },
                        }
                    }
                }
            })
        end

        -- nvim-cmp supports additional completion capabilities, so broadcast that to servers
        local capabilities = vim.lsp.protocol.make_client_capabilities()
        capabilities = require('cmp_nvim_lsp').default_capabilities(capabilities)

        -- Ensure the servers above are installed
        local mason_lspconfig = require 'mason-lspconfig'

        mason_lspconfig.setup {
            ensure_installed = vim.tbl_keys(servers),
        }

        mason_lspconfig.setup_handlers {
            function(server_name)
                require('lspconfig')[server_name].setup {
                    capabilities = capabilities,
                    on_attach = lsp.on_attach,
                    settings = servers[server_name],
                    filetypes = (servers[server_name] or {}).filetypes,
                }
            end,
        }
    end
}
