return {
    -- DAP - Debugging Adapter Protocol
    "mfussenegger/nvim-dap",
    dependencies = {
        -- UI for nvim-dap with sane defaults
        {
            "rcarriga/nvim-dap-ui",
            dependencies = {
                "nvim-neotest/nvim-nio",
            },
            opts = {},
        },

        -- virtual text for the debugger
        {
            "theHamsta/nvim-dap-virtual-text",
            opts = {},
        },
    },

    keys = {
        -- <leader>dd starts the debugger for rust (see rustaceanvim.lua)
        -- common keymaps
        { "<leader>db", function() require("dap").toggle_breakpoint() end, desc = "Toggle Breakpoint" },
        { "<leader>dc", function() require("dap").continue() end,          desc = "Continue" },
        { "<leader>dC", function() require("dap").run_to_cursor() end,     desc = "Run to Cursor" },
        { "<leader>di", function() require("dap").step_into() end,         desc = "Step Into" },
        { "<leader>do", function() require("dap").step_over() end,         desc = "Step Over" },
        { "<leader>dO", function() require("dap").step_out() end,          desc = "Step Out" },
        { "<leader>dl", function() require("dap").run_last() end,          desc = "Run Last" },
        { "<leader>dt", function() require("dap").terminate() end,         desc = "Terminate" },

        -- { "<leader>dB", function() require("dap").set_breakpoint(vim.fn.input('Breakpoint condition: ')) end, desc = "Breakpoint Condition" },
        -- { "<leader>da", function() require("dap").continue({ before = get_args }) end,                        desc = "Run with Args" },
        -- { "<leader>dg", function() require("dap").goto_() end,                                                desc = "Go to Line (No Execute)" },
        -- { "<leader>dj", function() require("dap").down() end,                                                 desc = "Down" },
        -- { "<leader>dk", function() require("dap").up() end,                                                   desc = "Up" },
        -- { "<leader>dp", function() require("dap").pause() end,                                                desc = "Pause" },
        -- { "<leader>dr", function() require("dap").repl.toggle() end,                                          desc = "Toggle REPL" },
        -- { "<leader>ds", function() require("dap").session() end,                                              desc = "Session" },
        -- { "<leader>dw", function() require("dap.ui.widgets").hover() end,                                     desc = "Widgets" },
    },

    config = function()
        -- automatically open/close dap-ui
        local dap, dapui = require("dap"), require("dapui")
        dap.listeners.before.attach.dapui_config = function()
            dapui.open()
        end
        dap.listeners.before.launch.dapui_config = function()
            dapui.open()
        end
        dap.listeners.before.event_terminated.dapui_config = function()
            dapui.close()
        end
        dap.listeners.before.event_exited.dapui_config = function()
            dapui.close()
        end
    end
}
