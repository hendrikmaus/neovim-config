# Neovim Configuration

Configuration for a hyperextensible Vim-based text editor.

*Source: https://neovim.io*

## Install

- Clone the repository to `.config/nvim`.
- Start Neovim

At this point, the package manager should automatically be installed
and continue with the setup.

## Conditional plugins

Some plugins are conditionally loaded.

- `NVIM_PLUGIN_AVANTE_ENABLED` https://github.com/yetone/avante.nvim
- `NVIM_LSP_NIXD_ENABLED` https://github.com/nix-community/nixd
  * requires `nixd` (lsp) and `alejandra` (formatter), e.g.
    + `nix profile install nixpkgs#nixd`
    + `nix profile install nixpkgs#alejandra`

## Troubleshooting

To test the setup in isolation:

```bash
cp -r ~/.config/nvim ~/.config/nvim-debug
alias nvim-debug='NVIM_APPNAME="nvim-debug" nvim'
```

With `NVIM_APPNAME=nvim-NAME` you can launch any config in isolation.

This will also create `~/.local/share/nvim-NAME` and `~/.local/state/nvim-NAME`.

## Contributing

Please mind: this a *personal* configuration.

> To start your own config see mentions below, especially
> [kickstart.nvim](https://github.com/nvim-lua/kickstart.nvim).

Open an issue to discuss your question or proposal before
putting time into a change-set.

Thank you.

## Mentions

Thank you to everyone who contributes to this ecosystem!

- Other configurations I drew inspiration from
  - https://github.com/Civitasv/runvim
  - https://github.com/nvim-lua/kickstart.nvim
  - https://github.com/dam9000/kickstart-modular.nvim
  - https://github.com/ThePrimeagen/neovimrc

## License

MIT

